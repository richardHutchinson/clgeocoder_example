//
//  ViewController.swift
//  CLGeocoderExample
//
//  Created by rhutchinson on 12/13/16.
//  Copyright © 2016 test. All rights reserved.
//

import UIKit
import CoreLocation //note: need to import the location framework.

class ViewController: UIViewController, CLLocationManagerDelegate {
    
    let locMgr = CLLocationManager()

    @IBOutlet weak var myLat: UILabel!
    @IBOutlet weak var myLong: UILabel!
    @IBOutlet weak var myAddressLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locMgr.desiredAccuracy = kCLLocationAccuracyBest
        locMgr.requestWhenInUseAuthorization()
        locMgr.startUpdatingLocation()
        locMgr.delegate = self
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        //note: the following prints the user's local information via the CLLocationManager (see CLGeocoder below for addtional human readable / user friendly information).
        //print(locations)
        
        let myCurrentLoc = locations[locations.count - 1]
        
        /*
         
            note: string interpolation - used due to the value returing a lat / long which = a double value type.
            note: the coordinate placeholder below represents the CLLocationCoordinate2D structure in Swift (lat / long).
            note: the simulator and log area will display lat / long numbers which change constantly; this is due to (make sure to click the simulator) Debug > Location > Freeway Drive. Personally I like to use the Debug > Location > Custom Location when testing.
         
        */
        myLat.text = "\(myCurrentLoc.coordinate.latitude)"
        myLong.text = "\(myCurrentLoc.coordinate.longitude)"
        
        // Geo Coder Example: Starts here.
        //note: myCurrentLoc below represents the user's coordinates (aka the user's CLLocation), which we will use to gather human readable / user friendly data. Thus, we will take those lat / long coordinates and gather information for the street, Zip, etc....
        CLGeocoder().reverseGeocodeLocation(myCurrentLoc) { (myPlacements, myError) in
            
            if myError != nil {
                
                // error message...
                
            }
            
            if let myPlacement = myPlacements?.first {
                
                //note: may want to check to see if the vlaue is nil; some locaitons will show up as nil due to many factors. Some of the common reasons are restrited areas in the world, uncharted, etc....
                let myAddress = "\(myPlacement.locality!) \(myPlacement.country!) \(myPlacement.postalCode!)"
                
                self.myAddressLabel.text = myAddress
                
            }
            
        }
        
    }

}
